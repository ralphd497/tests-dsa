import ballerina/http;
import ballerina/log;
import ballerina/uuid;
 
# Represents a Student
public type Student record {|
   # Student number
   string number?;
   # student name
   string name;
   # Student email address
   string email;
   # courses
   string courses;
|};
 
# An enum to represent courses
public enum courses {
   Web_development,
   Basic_science,
   Data_networks,
   English


}
 
# Represents courses
public type course record {|
   # course
   string courses;
   
|};
 
# Represents an error
public type Error record {|
   # Error code
   string code;
   # Error message
   string message;
|};
 
# Error response
public type ErrorResponse record {|
   # Error
   Error 'error;
|};
 
# Bad request response
public type ValidationError record {|
   *http:BadRequest;
   # Error response.
   ErrorResponse body;
|};
 
# Represents headers of created response
public type LocationHeader record {|
   # Location header. A link to the created Student record.
   string location;
|};
 
# Student record Creation response
public type StudentCreated record {|
   *http:Created;
   # Location header representing a link to the created Student record.
   LocationHeader headers;
|};
 
# Student updated response
public type StudentUpdated record {|
   *http:Ok;
|};
 
# The Student service
service /Student on new http:Listener(9090) {
 
   private map<Student> students = {};
 
   # List all Students
   # + return - List of Students
    resource function get students() returns Student[] {
       return self.students.toArray();
   }
 
   # Add a new Student
   #
   # + student - student to be added
   # + return - student created response or validation error
   resource function post products(@http:Payload Student student) returns StudentCreated|ValidationError {
       if student.name.length() == 0 || student.email.length() == 0 {
           log:printWarn("Student name or email is not present", student = student);
           return <ValidationError>{
               body: {
                   'error: {
                       code: "INVALID_NAME",
                       message: "Student name and email are required"
                   }
               }
           };
       }
 
 
       log:printDebug("Adding new Student", student = student);
       student.number = uuid:createType1AsString();
       self.students[<string>student.number] = student;
       log:printInfo("Added new student", student = student);
 
       string studentUrl = string `/students/${<string>student.number}`;
       return <StudentCreated>{
           headers: {
               location: studentUrl
           }
       };
   }
 
   # Update Student record
   #
   # + student - Student record updated
   # + return - A Student updated response or an error if the Student is invalid
   resource function put student(@http:Payload Student student) returns StudentUpdated|ValidationError {
       if student.number is () || !self.students.hasKey(<string>student.number) {
           log:printWarn("Invalid student provided for update", student = student);
           return <ValidationError>{
               body: {
                   'error: {
                       code: "INVALID_Student",
                       message: "Invalid Student"
                   }
               }
           };
       }
 
       log:printInfo("Updating student", student = student);
       self.students[<string>student.number] = student;
       return <StudentUpdated>{};
   }
 
   # Deletes Student record
   #
   # + number - Student number
   # + return - Deleted Student record or a validation error
   resource function delete students/[string number]() returns Student|ValidationError {
       if !self.students.hasKey(<string>number) {
           log:printWarn("Invalid Student number to be deleted", number = number);
           return {
               body: {
                   'error: {
                       code: "INVALID_number",
                       message: "Invalud Student number"
                   }
               }
           };
       }
 
       log:printDebug("Deleting Student record", number = number);
       Student removed = self.students.remove(number);
       log:printDebug("Deleted Student", Student = removed);
       return removed;
   }
}